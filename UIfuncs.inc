$NOLIST
CSEG
;---------------------------setting and displaying parameters-------------------------------------;
Load_Defaults: ; Load defaults if keys are incorrect 
	mov SoakTemp, #150
	mov SoakTime, #45
	mov ReflowTemp, #225
	mov ReflowTemp+1, #0
	mov ReflowTime, #30
	ret
	
Load_Potent_values:
	mov x, Potentiometer
	mov x+1, Potentiometer+1
	Load_y(150) ; choose max value
	lcall mul32
	Load_y(1000)
	lcall div32
	mov SoakTemp_2, x
	Load_x(0)
	Load_y(0)
	
	mov x, Potentiometer
	mov x+1, Potentiometer+1
	Load_y(90) ; choose max value
	lcall mul32
	Load_y(1000)
	lcall div32
	mov SoakTime_2, x
	Load_x(0)
	Load_y(0)
	
	mov x, Potentiometer
	mov x+1, Potentiometer+1
	Load_y(252) ; choose max value
	lcall mul32
	Load_y(1000)
	lcall div32
	mov ReflowTemp_2, x
	mov ReflowTemp_2+1, x+1
	Load_x(0)
	Load_y(0)
	
	mov x, Potentiometer
	mov x+1, Potentiometer+1
	Load_y(90) ; choose max value
	lcall mul32
	Load_y(1000)
	lcall div32
	mov ReflowTime_2, x
	Load_x(0)
	Load_y(0)
	
	ret
	
update_screen:

	mov a, screen			; case(screen)
	cjne a, #0, $+6	
		ljmp done_update
	cjne a, #1, $+6
		ljmp display_soak_time
	cjne a, #2, $+6
		ljmp display_soak_temp
	cjne a, #3, $+6
		ljmp display_reflow_time
	cjne a, #4, $+6
		ljmp display_reflow_temp
	cjne a, #5, $+6
		ljmp display_readingfrompython
	ljmp Print_Reflow_State		;default
	
	display_soak_temp:
		Set_Cursor(1,1)
		Send_Constant_String(#soak)
		Load_x(0)
		mov x, SoakTemp_2
		lcall hex2bcd 
		Set_Cursor(1,13)
		lcall Display_4_digit_BCD
		Set_Cursor(2,1)
		Send_Constant_String(#temperature)
		
		mov x, SoakTemp
		lcall hex2bcd
		Set_Cursor(2,13)
		lcall Display_4_digit_BCD
		
		lcall adjust_Param_SoakTemp
		
		Read_button(save_button, done_update)
		mov SoakTemp, SoakTemp_2
		
		
	ljmp done_update

	display_soak_time:
	
		Set_Cursor(1,1)
		Send_Constant_String(#soak)
		Load_x(0)
		mov x, SoakTime_2
		lcall hex2bcd 
		Set_Cursor(1,14)
		Display_BCD(bcd)
		Set_Cursor(2,1)
		Send_Constant_String(#time)
		
		Set_Cursor(2,14)
		mov x, SoakTime
		lcall hex2bcd
		Display_BCD(bcd)
		
		lcall adjust_Param_SoakTime
		Read_button(save_button, done_update1)
		
			mov SoakTime, SoakTime_2
		done_update1:
		ljmp done_update

	display_reflow_temp:
	
		Set_Cursor(1,1)
		Send_Constant_String(#reflow)
		Load_x(0)
		mov x, ReflowTemp_2
		mov x+1, ReflowTemp_2+1
		
		lcall hex2bcd 
		Set_Cursor(1,13)
		lcall Display_4_digit_BCD
		Set_Cursor(2,1)
		Send_Constant_String(#temperature)
		
		mov x, ReflowTemp
		mov x+1, ReflowTemp+1
		lcall hex2bcd
		Set_Cursor(2,13)
		lcall Display_4_digit_BCD
		
		lcall adjust_Param_ReflowTemp
		Read_button(save_button, done_update2)
			mov ReflowTemp, ReflowTemp_2
			mov ReflowTemp+1, ReflowTemp_2+1
			
		done_update2:
		ljmp done_update

	display_reflow_time:
	
		Set_Cursor(1,1)
		Send_Constant_String(#reflow)
		Load_x(0)
		mov x, ReflowTime_2
		lcall hex2bcd 
		Set_Cursor(1,14)
		Display_BCD(bcd)
		Set_Cursor(2,1)
		Send_Constant_String(#time)
		;Set_Cursor(2,11)
		;Display_BCD(ReflowTime_mins)
		Set_Cursor(2,14)
		mov x, ReflowTime
		lcall hex2bcd
		Display_BCD(bcd)
		
		lcall adjust_Param_ReflowTime
		Read_button(save_button, done_update3)
			mov ReflowTime, ReflowTime_2
		done_update3:
		ljmp done_update

	display_readingfrompython: 
		Set_Cursor(1,1)
		Send_Constant_String(#reading)
		Set_Cursor(2,1)
		Send_Constant_String(#from_python)	
		done_update4:
		ljmp done_update
		
	display_LCD_start:

		Set_Cursor(2,1)
		Display_BCD(temp) ;read temperature from thermo couple
		Set_Cursor(2,13)
		Send_Constant_String(#':')
		Set_Cursor(2,11)
		Display_BCD(RunningTime_mins)
		Set_cursor(2,14)
		Display_BCD(RunningTime_secs)
	ljmp done_update

	Print_Reflow_State:
		Set_Cursor(1,1)
		Send_Constant_String(#DisplayLine1)
		Set_Cursor(2,1)
		Send_Constant_String(#DisplayLine2)
		Set_Cursor(1,6)
		load_x(0)
		mov x, temp
		mov x+1, temp+1
		lcall hex2bcd
		lcall Display_4_digit_BCD
		Set_Cursor(1,15)
		Display_BCD(state)
		Set_Cursor(2,9)
		mov x, runtime
		mov x+1, runtime+1
		lcall hex2bcd
		lcall Display_4_digit_BCD

	done_update:
	ret
	
adjust_Param_SoakTemp:
	
	check_inc:
	Read_button(inc_button, check_dec)
	clr a
	mov a, SoakTemp
	cjne a, #150, continue_incSoakTemp
	mov a, #0
	sjmp displaySoakTemp

	continue_incSoakTemp:
	add a, #0x01

	displaySoakTemp:
	
	mov x, a
	mov SoakTemp, a
	lcall hex2bcd 
	Set_Cursor(2,13)
	lcall Display_4_digit_BCD
	jb inc_button, check_inc
	
	check_dec:
	Read_button(dec_button, check_adjust)
	clr a
	mov a, SoakTemp
	subb a, #1
	mov x, a
	mov SoakTemp, a
	lcall hex2bcd 
	Set_Cursor(2,13)
	lcall Display_4_digit_BCD
	jb dec_button, check_dec
	
	check_adjust:
	ret
	
adjust_Param_SoakTime:
	
	check_inc1:
	Read_button(inc_button, check_dec1)
	clr a
	mov a, SoakTime
	cjne a, #100, continue_inc
	mov a, #0
	sjmp displaySoakTime
	continue_inc:
	add a, #0x01
	
	displaySoakTime:
	mov x, a
	mov SoakTime, a
	lcall hex2bcd 
	Set_Cursor(2,14)
	lcall Display_4_digit_BCD
	jb inc_button, check_inc1
	
	check_dec1:
	Read_button(dec_button, check_adjust1)
	clr a
	mov a, SoakTime
	subb a, #1
	mov x, a
	mov SoakTime, a
	lcall hex2bcd 
	Set_Cursor(2,14)
	lcall Display_4_digit_BCD
	jb dec_button, check_dec1
	
	check_adjust1:
	ret
	
	adjust_Param_ReflowTemp:
	
	check_inc2:
	Read_button(inc_button, check_dec2)
	
	Load_x(0)
	mov x, ReflowTemp
	mov x+1, ReflowTemp+1

	mov a, x+1
	cjne a, #1, continue_incReflowTemp
	Load_x(0)
	sjmp displayReflowTemp

	continue_incReflowTemp:
	Load_y(1)
	lcall add32

	displayReflowTemp:
	clr a
	mov ReflowTemp, x
	mov ReflowTemp+1, x+1
	lcall hex2bcd 
	Set_Cursor(2,13)
	lcall Display_4_digit_BCD
	jb inc_button, check_inc2
	
	check_dec2:
	Read_button(dec_button, check_adjust2)
		load_x(0)
		mov x, ReflowTemp
		mov x+1, ReflowTemp+1
		load_y(1)
		lcall sub32
		mov ReflowTemp, x
		mov ReflowTemp+1, x+1
		lcall hex2bcd 
		Set_Cursor(2,13)
		lcall Display_4_digit_BCD
		jb dec_button, check_dec2
	
	check_adjust2:
	ret
	
	adjust_Param_ReflowTime:
	
	check_inc3:
	Read_button(inc_button, check_dec3)
	clr a
	mov a, ReflowTime

	cjne a, #100, continue_incReflowTime
	mov a, #0
	sjmp displayReflowTime

	continue_incReflowTime:
	add a, #0x01

	displayReflowTime:
	
	mov x, a
	mov ReflowTime, a
	lcall hex2bcd 
	Set_Cursor(2,14)
	lcall Display_4_digit_BCD
	jb inc_button, check_inc3
	
	check_dec3:
	Read_button(dec_button, check_adjust3)
	clr a
	mov a, ReflowTime
	subb a, #1
	mov x, a
	mov ReflowTime, a
	lcall hex2bcd 
	Set_Cursor(2,14)
	lcall Display_4_digit_BCD
	jb dec_button, check_dec3
	
	check_adjust3:
	ret

Read_inputs:
	Read_button(start_button, no_start) 
		mov screen, #6
		ljmp start_FSM
	no_start:
	
	Read_button(param_button, no_param)
		Set_Cursor(1,1)
		Send_Constant_String(#Clear)
		Set_Cursor(2,1)
		Send_Constant_String(#Clear)
		mov a, screen 	;screen++
		inc a
		mov screen, a
		cjlt(a, #6, skipsdfs)	;if (screen >= 6) {screen = 1;}
			mov screen, #1
		skipsdfs:
	no_param:
	
	ret
$LIST