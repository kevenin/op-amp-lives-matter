$NOLIST
;requires a desired percentage duty cycle variable
;	called pcent_pow
;	binary
;	between 0 and 100
;add an interrupt service routine
;the period of the square wave will be stored in the constant
;	called modwave_period
;the finished program will be a function
;with calling format
;	lcall pulse_pow
;	will be called at a rapid rate every modwave_period seconds by timer0
;------------------------------------;
; pseudocode:                        ;
; start                              ;
; 	turn on output pin               ;
;	wait period * %duty cycle        ;
;   turn off output pin              ;
; reti                               ;
;------------------------------------;


CSEG

; total period = 1ms, pwm/100*1000 gives # of microseconds to wait
sqwave_modulate: 
	setb SSR_pin
	;calculating how long wave stays on = pwm*10
	load_x(0)
	mov x, pwm
	;load_y(10)
	;lcall mul32
	
	mov R1, x
	wait_loop:	; wait x microseconds
		Wait_10xMicro_Seconds(#1)
		djnz R1, wait_loop
	clr SSR_pin 
	reti
	


$LIST