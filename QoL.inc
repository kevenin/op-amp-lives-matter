$NOLIST
CSEG
; usage: ccallne( value, compare, function )
; eg. ccallne(a, #25, safe_temp)
ccallne mac
	cjne %0, %1, $+5	;3 bytes
	sjmp $+5			;2 bytes
	lcall %2			;3 bytes
endmac

cljne mac
	cjne %0, %1, $+5	;3 bytes
	sjmp $+5			;2 bytes
	ljmp %2				;3 bytes
endmac

; usage: cjeq(value, compare, label)
cjeq mac
	cjne %0, %1, $+6	;3 bytes
	ljmp %2				;3 bytes
endmac

; usage: cjlt(value, compare, label)
cjlt mac
	clr c
	subb %0, %1
	jc %2
endmac
cjgt mac
	clr c
	mov b, %0
	mov a, %1
	subb a, b
	jc %2
endmac
cjge mac
	clr c
	subb %0, %1
	jnc %2
endmac
cjle mac
	clr c
	mov b, %0
	mov a, %1
	subb a, b
	jnc %2
endmac

; usage: callnb(bit, function)
callnb mac
	jb %0, $+6			;3 bytes
	lcall %1			;3 bytes
endmac

; usage: callnb(bit, function)
callb mac
	jnb %0, $+6			;3 bytes
	lcall %1			;3 bytes
endmac

;---------------------------------;
; Send a BCD number to PuTTY      ;
;---------------------------------;
Send_BCD mac
	push ar0
	mov r0, %0
	lcall ?Send_BCD
	pop ar0
endmac

?Send_BCD:
	push acc
	; Write most significant digit
	mov a, r0
	swap a
	anl a, #0fh
	orl a, #30h
	lcall putchar
	; write least significant digit
	mov a, r0
	anl a, #0fh
	orl a, #30h
	lcall putchar
	pop acc
	ret

Display_10_digit_BCD:
	Set_Cursor(2, 7)
	Display_BCD(bcd+4)
	Display_BCD(bcd+3)
	Display_BCD(bcd+2)
	Display_BCD(bcd+1)
	Display_BCD(bcd+0)
	ret
	
Display_4_digit_BCD:
	Display_BCD(bcd+1)
	Display_BCD(bcd+0)
	ret

Send_10_digit_BCD:
	Send_BCD(bcd+4)
	Send_BCD(bcd+3)
	Send_BCD(bcd+2)
	Send_BCD(bcd+1)
	Send_BCD(bcd+0)
	mov a, #'\r'
	lcall putchar
	mov a, #'\n'
	lcall putchar
	ret

	; Send a character using the serial port
putchar:
    jnb TI, putchar
    clr TI
    mov SBUF, a
    ret

; Send a constant-zero-terminated string using the serial port
SendString:
    clr A
    movc A, @A+DPTR
    jz SendStringDone
    lcall putchar
    inc DPTR
    sjmp SendString
SendStringDone:
    ret
	

INIT_SPI:
	setb MY_MISO ; Make MISO an input pin
	clr MY_SCLK ; For mode (0,0) SCLK is zero
	ret

DO_SPI_G:
	push acc
	mov R1, #0 ; Received byte stored in R1
	mov R2, #8 ; Loop counter (8-bits)
DO_SPI_G_LOOP:
	mov a, R0 ; Byte to write is in R0
	rlc a ; Carry flag has bit to write
	mov R0, a
	mov MY_MOSI, c
	setb MY_SCLK ; Transmit
	mov c, MY_MISO ; Read received bit
	mov a, R1 ; Save received bit in R1
	rlc a
	mov R1, a
	clr MY_SCLK
	djnz R2, DO_SPI_G_LOOP
	pop acc
	ret
	
SendSerial mac
	mov DPTR, %0
    lcall SendString
endmac

; Usage: 
; Read_button(button, end_label)
; 	some code
; end_label:
; some code will only be run if the button is pressed at the moment of execution
; must be looped
; has debounce built in
; main program will be halted until the button has been released
Read_button mac
	jb %0, %1_vec  ; if the button is not pressed skip
	Wait_Milli_Seconds(#30)	; Debounce delay.  This macro is also in 'LCD_4bit.inc'
	jb %0, %1_vec  ; if the button is not pressed skip
	jnb %0, $  ; loop until release
	sjmp %1_skip
%1_vec:
	ljmp %1
%1_skip:
endmac  

reset_runtime:
	mov halfruntime, #0
	mov halfruntime+1, #0
	mov runtime, #0
	mov runtime+1, #0
	ret

reset_timer:
	mov a, #0
	mov halfsecs, a
	mov sec, a
	ret

inc_runtime:
	Load_x(0)
	mov x, halfruntime
	mov x+1, halfruntime+1
	Load_y(1)
	lcall add32
	mov halfruntime, x
	mov halfruntime+1, x+1
	
	Load_Y(2)
	lcall div32
	
	mov runtime, x
	mov runtime+1, x+1
	ret
	
inc_timer:
	mov a, halfsecs
	inc a
	mov halfsecs, a
	
	Load_x(0)
	mov x, halfsecs
	Load_Y(2)
	lcall div32
	
	mov sec, x
	ret

$LIST