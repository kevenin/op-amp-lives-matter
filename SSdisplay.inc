;converts BCD numbers to 7 segment display commands
;this will be a function
;may require some pin variables
;requires a library for converting digits to pin combinations
$NOLIST
CSEG
;------------------------------------;
; Pseudocode:                        ;
; start                              ;
; 	BCD_to_7pin                      ;
;	select first digit               ;
;	turn on first digit power        ;
;	turn on first digit pin set      ;
;	turn off first digit power       ;
;	turn off first digit pin set     ;
;	repeat for other digits          ;
; ret                                ;
;------------------------------------;

;Write macros for the ten numbers to turn on the bits for the segment

;Variables
; Disp1,2,3 for displays
; bcd, x, y
; temp for reading from thermal couple


SEGA equ P0.4
SEGB equ P0.2
SEGC equ P2.6
SEGD equ P4.5
SEGE equ P0.7
SEGF equ P0.3
SEGG equ P0.6
SEGP equ P2.7
CA1  equ P0.0
CA2  equ P0.1
CA3  equ P0.5


; Pattern to load passed in accumulator
load_segments:
	mov c, acc.0
	mov SEGA, c
	mov c, acc.1
	mov SEGB, c
	mov c, acc.2
	mov SEGC, c
	mov c, acc.3
	mov SEGD, c
	mov c, acc.4
	mov SEGE, c
	mov c, acc.5
	mov SEGF, c
	mov c, acc.6
	mov SEGG, c
	mov c, acc.7
	mov SEGP, c
	ret

Update_7_seg:
	
	; The two registers used in the ISR must be saved in the stack
	push acc
	push psw

;;;  State machine for 7-segment displays starts here
	; Turn all displays off
	setb CA1
	setb CA2
	setb CA3

	mov a, SSegState
SSegState0:
	cjne a, #0, SSegState1
	mov a, disp1
	lcall load_segments
	clr CA1
	inc SSegState
	sjmp SSegState_done
SSegState1:
	cjne a, #1, SSegState2
	mov a, disp2
	lcall load_segments
	clr CA2
	inc SSegState
	sjmp SSegState_done
SSegState2:
	cjne a, #2, SSegState_reset
	mov a, disp3
	lcall load_segments
	clr CA3
	mov SSegState, #0
	sjmp SSegState_done
SSegState_reset:
	mov SSegState, #0
SSegState_done:

	pop psw
	pop acc
	ret

HEX_7SEG: DB 0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90

Change_7_seg:
	Load_x(0)
	
	mov x, temp
	mov x+1, temp+1
	mov x+2, temp+2
	mov x+3, temp+3

;	Load_x(236)
	lcall hex2bcd

	mov dptr, #HEX_7SEG
	mov a, BCD+0
	anl a, #0x0f
	movc a, @a+dptr
	mov disp1, a
	mov a, BCD+0
	swap a
	anl a, #0x0f
	movc a, @a+dptr
	mov disp2, a
	mov a, BCD+1
	anl a, #0x0f 
	movc a, @a+dptr
	mov disp3, a

	ret

$LIST



