; ISR_example.asm: a) Increments/decrements a BCD variable every half second using
; an ISR for timer 2; b) Generates a 2kHz square wave at pin P3.7 using
; an ISR for timer 0; and c) in the 'main' loop it displays the variable
; incremented/decremented using the ISR for timer 2 on the LCD.  Also resets it to 
; zero if the 'BOOT' pushbutton connected to P4.5 is pressed.
$NOLIST
$MODLP51
$include(LCD_4bit.inc)
$LIST

; There is a couple of typos in MODLP51 in the definition of the timer 0/1 reload
; special function registers (SFRs), so:

TIMER0_RELOAD_L DATA 0xf2
TIMER1_RELOAD_L DATA 0xf3
TIMER0_RELOAD_H DATA 0xf4
TIMER1_RELOAD_H DATA 0xf5

DOOR_SIGNALN   	equ P0.0
MOTOR_OPEN		equ P0.1
MOTOR_CLOSE		equ P0.2

; Reset vector
org 0x0000
    ljmp main

; External interrupt 0 vector (not used in this code)
org 0x0003
	reti

; Timer/Counter 0 overflow interrupt vector
org 0x000B
	ljmp Timer0_ISR

; External interrupt 1 vector (not used in this code)
org 0x0013
	reti

; Timer/Counter 1 overflow interrupt vector (not used in this code)
org 0x001B
	reti

; Serial port receive/transmit interrupt vector (not used in this code)
org 0x0023 
	reti
	
; Timer/Counter 2 overflow interrupt vector
org 0x002B
	ljmp Timer2_ISR

; In the 8051 we can define direct access variables starting at location 0x30 up to location 0x7F
dseg at 0x30
Count1ms:     ds 2 ; Used to determine when half second has passed
BCD_counter:  ds 1 ; The BCD counter incrememted in the ISR and displayed in the main loop

; In the 8051 we have variables that are 1-bit in size.  We can use the setb, clr, jb, and jnb
; instructions with these variables.  This is how you define a 1-bit variable:
bseg
oven_open: dbit 1

cseg

Timer0_Init:
	ret


Timer0_ISR:

	reti

Timer2_Init:
	ret
	
Timer2_ISR:
	reti

;---------------------------------;
; Main program. Includes hardware ;
; initialization and 'forever'    ;
; loop.                           ;
;---------------------------------;
main:
	; Initialization
    mov SP, #0x7F
    lcall Timer0_Init
    lcall Timer2_Init
    ; In case you decide to use the pins of P0, configure the port in bidirectional mode:
    mov P0M0, #0
    mov P0M1, #0
	
	clr oven_open
	
	; After initialization the program stays in this 'forever' loop
loop:
	jb oven_open, open_state
		sjmp closed_state
	open_state:
		jnb DOOR_SIGNAL, open_state
		setb MOTOR_OPEN
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		clr MOTOR_OPEN
		setb oven_open
	closed_state:
		jnb DOOR_SIGNAL, closed_state
		setb MOTOR_CLOSE
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		Wait_Milli_Seconds(#250)
		clr MOTOR_CLOSE
		clr oven_open
    ljmp loop
END