import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys, time, math
import serial
import serial.tools.list_ports
PORT = 'COM9'
xsize=650

try:
 ser.close();
except:
 print();
try:
 ser = serial.Serial(PORT, 115200, timeout=100)
except:
 print ('Serial port %s is not available' % PORT);
 portlist=list(serial.tools.list_ports.comports())
 print('Trying with port %s' % portlist[0][0]);
 ser = serial.Serial(portlist[0][0], 115200, timeout=100)
ser.isOpen()

   
def data_gen():
    t = data_gen.t
    maxVal =0
    plt.figtext(0.05,0.92, 'High:' , fontsize = 10, bbox=dict(facecolor='wheat'))
    txtVar = plt.figtext(0.15,0.92, maxVal , fontsize = 10, bbox=dict(facecolor='wheat'))
    while True:
        ser.readline(); #attempt to scrap first value
        strin = ser.readline();
        strin = float(strin) 
        t+=1
        val = int((strin))
        yield t, val
        if(t%60 == 0):
            ax.annotate(strin, xy=(t,   val+3),     xytext=(t,   val+5), arrowprops=dict(facecolor='red', shrink=50,headwidth=10,width=8))
        if(strin >= maxVal):
            maxVal = strin
            for txt in fig.texts:
                txt.set_visible(False)
            plt.figtext(0.05,0.92, 'High:' , fontsize = 10, bbox=dict(facecolor='wheat'))
            txtVar = plt.figtext(0.15,0.92, maxVal , fontsize = 10, bbox=dict(facecolor='wheat'))
            txtVar.set_visible(True)

def run(data):
    # update the data
    t,y = data
    if t>-1:
        xdata.append(t)
        ydata.append(y)
        if t>xsize: # Scroll to the left.
            ax.set_xlim(t-xsize, t)
        line.set_data(xdata, ydata)

    return line,

def on_close_figure(event):
    sys.exit(0)
    
#maxVal = 0
data_gen.t = -1
fig = plt.figure()
fig.canvas.mpl_connect('close_event', on_close_figure)
ax = fig.add_subplot(111)
line, = ax.plot([], [], lw=2)
ax.set_ylim(0, 300)
ax.set_xlim(0, xsize)
plt.title('Temperature vs Time')
plt.xlabel('Time (s)')
plt.ylabel('Temperature ºC')
ax.grid()
xdata, ydata = [], []
ax.set_facecolor('wheat')
fig.canvas.set_window_title('Temperature vs Time')
#plt.figtext(0.15,0.92, maxVal , fontsize = 10, bbox=dict(facecolor='wheat', alpha=0.5))

# Important: Although blit=True makes graphing faster, we need blit=False to prevent
# spurious lines to appear when resizing the stripchart.
ani = animation.FuncAnimation(fig, run, data_gen, blit=False, interval=100, repeat=False)
plt.show()
