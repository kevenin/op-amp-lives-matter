
$MODLP51

CLK  EQU 22118400
BAUD equ 115200
BRG_VAL equ (0x100-(CLK/(16*BAUD)))

TIMER0_RELOAD_L DATA 0xf2
TIMER1_RELOAD_L DATA 0xf3
TIMER0_RELOAD_H DATA 0xf4
TIMER1_RELOAD_H DATA 0xf5

; These 'EQU' must match the wiring between the microcontroller and ADC

CE_ADC EQU P2.0
MY_MOSI EQU P2.1
MY_MISO EQU P2.2
MY_SCLK EQU P2.3

LCD_RS equ P1.1
LCD_RW equ P1.2
LCD_E  equ P1.3
LCD_D4 equ P3.2
LCD_D5 equ P3.3
LCD_D6 equ P3.4
LCD_D7 equ P3.5

save_button equ P2.7
Abort_button equ p2.4
Param_button equ P2.4
start_button equ P2.5
inc_button equ P1.0
dec_button equ P4.4
SSR_pin equ P3.7
SPEAKER equ P3.6

TIMER0_RATE   EQU 1000     ; 1000Hz squarewave
TIMER0_RELOAD EQU ((65536-(CLK/TIMER0_RATE)))
TIMER2_RATE   EQU 1000     ; 1000Hz, for a timer tick of 1ms
TIMER2_RELOAD EQU ((65536-(CLK/TIMER2_RATE)))

; Reset vector
org 0x0000
    ljmp MainProgram

; External interrupt 0 vector (not used in this code)
org 0x0003
	reti

; Timer/Counter 0 overflow interrupt vector
org 0x000B
	ljmp Timer0_ISR

; External interrupt 1 vector (not used in this code)
org 0x0013
	reti

; Timer/Counter 1 overflow interrupt vector 
org 0x001B
	reti

; Serial port receive/transmit interrupt vector (not used in this code)
org 0x0023 
	reti
	
; Timer/Counter 2 overflow interrupt vector
org 0x002B
	ljmp Timer2_ISR
	
dseg at 0x30
Count1ms:     ds 2 ; Used to determine when half second has passed
Count1ms_0:	  ds 1 ; Use for PWM
Result:		  ds 2 ; Used to store temperature
x:			  ds 4 ; variable x
y:			  ds 4 ; variable y
bcd:		  ds 5 ; 10 digit bcd
sum: ds 4
Duty_Cycle: ds 2 ; variable Duty Cycle
ref_temp: ds 4

; Variables for Soak time / temp and Reflow time / temp
SoakTemp: ds 1
SoakTime: ds 1
ReflowTemp: ds 2
ReflowTime: ds 1

SoakTemp_2: ds 1
SoakTime_2: ds 1
ReflowTemp_2: ds 2
ReflowTime_2: ds 1

Potentiometer: ds 2

; Variables for 7-segment state machine
Disp1:  ds 1
Disp2:  ds 1
Disp3:  ds 1
SSegState: ds 1
screen: ds 1
state:  ds 1 ; variable for state
stateCounter: ds 1

sec: ds 1
halfsecs: ds 1
runtime: ds 2
halfruntime: ds 2

pwm: ds 1 ; power variable in reflow state machine
temp: ds 4 ; temperature variable in reflow state machine

RunningTime_mins: ds 1 ; *calculate running time 
RunningTime_secs: ds 1 ; *calculate running time
ReflowTime_mins: ds 1
ReflowTime_secs: ds 1 
SoakTime_mins: ds 1
SoakTime_secs: ds 1

bseg
mf: dbit 1
half_seconds_flag: dbit 1
speaker_on: dbit 1
skippy: dbit 1

Clear: db '                ', 0
temperature: db 'Temperature ', 0
time: db 'Time            ', 0
soak: db 'Soak            ', 0
reflow: db 'Reflow          ', 0
Preheat1: db 'Pre-heat 1', 0
Preheat2: db 'Pre-heat 2', 0
Cooling: db 'Cooling', 0
Hello_World:   DB '\r', '\n', 0
reading: db 	'Reading         ', 0
from_python: db 'From Python...  ', 0

DisplayLine1: db 'Temp:           ', 0
DisplayLine2: db 'Runtime:        ', 0

CSEG

$NOLIST
$include(math32.inc)
$include(LCD_4bit.inc)

$include(QoL.inc)
$include(SSdisplay.inc)
$include(sqwave_modulate.inc)
$include(ThermocoupleCode.inc)
$include(UIfuncs.inc)
$include(pyread.inc)
$LIST

; if temperature is safe to touch
safe_temp:
	mov R3, #6  
	Six_beep: setb speaker_on
	Wait_Milli_Seconds(#250)
	Wait_Milli_Seconds(#250)
	clr speaker_on
	Wait_Milli_Seconds(#250)
	Wait_Milli_Seconds(#250)
	djnz R3, Six_beep
	
	ret

; if stop (start) button is pressed, goes to emergency stop state and resets to state 0
state_STOP: 
	setb speaker_on
	Wait_Milli_Seconds(#250)
	Wait_Milli_Seconds(#250)
	clr speaker_on
	mov a, #0
	mov state, a
	ljmp state0
;-----------------------------------------------------------------------------------------------------

;---------------------------------;
; Routine to initialize the ISR   ;
; for timer 0                     ;
;---------------------------------;
Timer0_Init:
	mov a, TMOD
	anl a, #0xf0 ; Clear the bits for timer 0
	orl a, #0x01 ; Configure timer 0 as 16-timer
	mov TMOD, a
	mov TH0, #high(TIMER0_RELOAD)
	mov TL0, #low(TIMER0_RELOAD)
	mov count1ms_0, #0
	; Set autoreload value
	mov TIMER0_RELOAD_H, #high(TIMER0_RELOAD)
	mov TIMER0_RELOAD_L, #low(TIMER0_RELOAD)
	; Enable the timer and interrupts
    setb ET0  ; Enable timer 0 interrupt
    setb TR0  ; Start timer 0
	ret

Timer0_ISR:
	push acc
	push psw
	clr c
	inc count1ms_0
	mov a, count1ms_0
	subb a, pwm
	;cpl c
	mov SSR_pin, c ; once the pulse has lasted pwm milliseconds, turn off
	mov a, count1ms_0
	subb a, #100 ; if time has reached 100ms, reset
	jc SSR_done
	setb SSR_pin
	mov count1ms_0, #0
SSR_done:
	pop psw
	pop acc
	reti

Timer2_Init:
	mov T2CON, #0 ; Stop timer/counter.  Autoreload mode.
	mov TH2, #high(TIMER2_RELOAD)
	mov TL2, #low(TIMER2_RELOAD)
	; Set the reload value
	mov RCAP2H, #high(TIMER2_RELOAD)
	mov RCAP2L, #low(TIMER2_RELOAD)
	; Init One millisecond interrupt counter.  It is a 16-bit variable made with two 8-bit parts
	clr a
	mov Count1ms+0, a
	mov Count1ms+1, a
	; Enable the timer and interrupts
    setb ET2  ; Enable timer 2 interrupt
    setb TR2  ; Enable timer 2
	ret

Timer2_ISR:
	clr TF2  ; Timer 2 doesn't clear TF2 automatically. Do it in ISR
	
	; if speaker is on, pulse the speaker
	jnb speaker_on, speaker_bypass
		cpl SPEAKER
		sjmp qqqqqq
	speaker_bypass:
		clr SPEAKER
	qqqqqq:
	; The two registers used in the ISR must be saved in the stack
	push acc
	push psw
	
	lcall Update_7_seg	
	
	; Increment the 16-bit one mili second counter
	inc Count1ms+0    ; Increment the low 8-bits first
	mov a, Count1ms+0 ; If the low 8-bits overflow, then increment high 8-bits
	jnz Inc_Done
	inc Count1ms+1
Inc_Done:
	; Check if half second has passed
	mov a, Count1ms+0
	cjne a, #low(500), Timer2_ISR_done ; Warning: this instruction changes the carry flag!
	mov a, Count1ms+1
	cjne a, #high(500), Timer2_ISR_done
	
	; 200 milliseconds have passed.  Set a flag so the main program knows
	setb half_seconds_flag ; Let the main program know half second had passed
	; Reset to zero the milli-seconds counter, it is a 16-bit variable
	clr a
	mov Count1ms+0, a
	mov Count1ms+1, a
		
Timer2_ISR_done:
	pop psw
	pop acc
	reti
	
; Configure the serial port and baud rate
InitSerialPort:
    ; Since the reset button bounces, we need to wait a bit before
    ; sending messages, otherwise we risk displaying gibberish!
    mov R1, #222
    mov R0, #166
    djnz R0, $   ; 3 cycles->3*45.21123ns*166=22.51519us
    djnz R1, $-4 ; 22.51519us*222=4.998ms
    ; Now we can proceed with the configuration
	orl	PCON,#0x80
	mov	SCON,#0x52
	mov	BDRCON,#0x00
	mov	BRL,#BRG_VAL
	mov	BDRCON,#0x1E ; BDRCON=BRR|TBCK|RBCK|SPD;
    ret

	NotPreHeat1:	
	add a, #0x01
	da a
	mov stateCounter, a
    mov a, stateCounter
	
Set_SoakTemp: 
    clr a 
    mov SoakTemp, #0x25 ; b/c value in BCD_counter gets displayed on lcd 
    mov a, SoakTemp
    add a, #0x01
    da a 
    ccallne( a, #240, display_soak_time)	;example
    mov SoakTemp, a
    ret

Set_ReflowTemp:
    clr a 
    mov ReflowTemp, #0x25 ; b/c value in BCD_counter gets displayed on lcd 
    mov a, ReflowTemp
    add a, #0x01
    da a 
    ccallne( a, #240, display_reflow_temp) ; cjne a, #240, branch_to_lcd2
    mov ReflowTemp, a
	ret

Set_ReflowTime: 
;seconds
    Timer2_ISR_da_reflow: 
	da a ; Decimal adjust instruction.  Check datasheet for more details!
	mov ReflowTime_secs, a
	ccallne( a, #60, display_reflow_time)
    mov ReflowTime_secs, a
;minutes
    mov ReflowTime_mins, #0
	clr a
	mov a, ReflowTime_mins
	add a, #0x01
	da a
	mov ReflowTime_mins, a
	ccallne( a, #60, display_reflow_time)
    mov ReflowTime_mins, a
	ret

Set_SoakTime: 
;seconds
    Timer2_ISR_soak: 
	da a ; Decimal adjust instruction.  Check datasheet for more details!
	mov SoakTime_secs, a
	ccallne( a, #60, display_soak_time)
    mov SoakTime_secs, a
;minutes
    mov SoakTime_mins, #0
	clr a
	mov a, SoakTime_mins
	add a, #0x01
	da a
	mov SoakTime_mins, a
	ccallne( a, #60, display_soak_time)
    mov SoakTime_mins, a
	ret

;--------------------------------------------------------------------------------------------------;
MainProgram:
    mov SP, #7FH ; Set the stack pointer to the begining of idata
    lcall InitSerialPort	;SPI and serial
	lcall INIT_SPI
	lcall Timer2_Init	; Timer 2 init
	lcall Timer0_Init
	mov P2M0, #0	; SPI pin init
    mov P2M1, #0 
	
	mov AUXR, #00010001B ; For p4.4
	
	setb EA   ; Enable Global interrupts
	lcall LCD_4BIT	; LCD screen setup
    mov pwm, #1
	clr speaker_on
	setb CA1
	setb CA2
	setb CA3
	mov temp, #0
	mov temp+1, #0

	lcall Load_Defaults
	mov screen, #0

    button_loop:
	;----------------------------------button checks-----------------------------------------------------;
	;loop for checking buttons continuously
	;screen states:
		;0 = initial
		;1 = soak temp
		;2 = ...
		;5 = started
		;6 = reading from python
		
		lcall read_inputs
		lcall update_screen
		lcall ReadPotent ; get potentiometer value
		lcall Load_Potent_values ; calculate soak/temp values using potentiometer 
		
		
		mov a, screen
		cjne a, #5, button_loop
			Wait_Milli_Seconds(#10)
			lcall poll_params
			mov screen, #1
	ljmp button_loop
	
	start_FSM:
	;------------------------------------------------------------------------------------------;

	lcall reset_timer
	lcall reset_runtime
	mov state, #0
	
	;-----------------------;;; **** FSM for reflow states *** ;;;;;---------------------------;
	FSM: 
	
		
		clr speaker_on
		Read_button(Abort_button, no_emergency)
			setb speaker_on	; beep
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
			mov state, #0x0
			mov pwm, #1
			ljmp button_loop
		No_emergency:
		
		jnb half_seconds_flag, continue_fsm
			lcall inc_timer
			lcall inc_runtime
			lcall Average_ADC_Channel
			lcall Change_7_seg
			lcall SetRoomTemp
			clr half_seconds_flag
			lcall update_screen
		continue_fsm:
		
		mov a, state
		
	state0: 
		cjne a, #0, state1
		mov pwm, #1
		lcall reset_runtime
		read_button(start_button, state0_done)
			mov state, #1	; goto state 1
			mov screen, #6
			setb speaker_on	; beep
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
	state0_done:
		ljmp FSM
		
	state1:
		cjne a, #1, state2
		mov pwm, #99
		
		mov a, runtime
		cjle(a, #60, abort_ok)
			mov a, temp
			cjge(a, #50, abort_ok)
				mov state, #0
				setb speaker_on
				Wait_Milli_Seconds(#50)
				clr speaker_on
				Wait_Milli_Seconds(#50)
				setb speaker_on
				Wait_Milli_Seconds(#50)
				clr speaker_on
				sjmp state0
		abort_ok:
		mov a, temp
		cjlt(a, soaktemp, state1_done)
			mov state, #2
			lcall reset_timer
			setb speaker_on
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
	state1_done:
		ljmp FSM

	state2:
		cjne a, #2, state3
		mov pwm, #20
		mov a, sec
		cjlt(a, soaktime, state2_done)	; constant used
			mov state, #3
			setb speaker_on
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
	state2_done:
		ljmp FSM

	state3:
		cjne a, #3, state4
		mov pwm, #99
		mov a, temp
		cjlt(a, reflowtemp, state3_done)	; constant used
			mov state, #4
			lcall reset_timer		
			setb speaker_on
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
	state3_done:
		ljmp FSM
	
	state4:
		cjne a, #4, state5
		mov pwm, #20
		mov a, sec
		cjlt(a, reflowtime, state4_done)	; constant used
			mov state, #5
			setb speaker_on
			Wait_Milli_Seconds(#250)
			Wait_Milli_Seconds(#250)
			clr speaker_on
	state4_done:
		ljmp FSM
	
	state5:
		cjne a, #5, state5_done
		mov pwm, #0
		mov a, temp
		cjge(a, #60, state5_done)
			mov state, #6
			setb speaker_on ;turn buzzer on
			mov a, #0
			mov R3, #9
			Long_beep: Wait_Milli_Seconds(#250)
			djnz R3, Long_beep
			clr speaker_on
	state5_done:
		ljmp FSM
		
	state6:
		cjne a, #6, state6_done
		mov a, temp
		cjge(a, #40, state6_done)
			;lcall safe_temp
			setb speaker_on
			Wait_Milli_Seconds(#200)
			clr speaker_on
			Wait_Milli_Seconds(#100)
			setb speaker_on
			Wait_Milli_Seconds(#250)
			clr speaker_on
			mov state, #0
			Wait_Milli_Seconds(#250)
	state6_done:
		ljmp FSM

	ljmp button_loop	;main loop
END



