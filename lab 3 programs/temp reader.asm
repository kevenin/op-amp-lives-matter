$MODLP51

CLK  EQU 22118400
BAUD equ 115200
BRG_VAL equ (0x100-(CLK/(16*BAUD)))

; These 'EQU' must match the wiring between the microcontroller and ADC
CE_ADC EQU P2.0
MY_MOSI EQU P2.1
MY_MISO EQU P2.2
MY_SCLK EQU P2.3

LCD_RS equ P1.1
LCD_RW equ P1.2
LCD_E  equ P1.3
LCD_D4 equ P3.2
LCD_D5 equ P3.3
LCD_D6 equ P3.4
LCD_D7 equ P3.5

TIMER2_RATE   EQU 1000     ; 1000Hz, for a timer tick of 1ms
TIMER2_RELOAD EQU ((65536-(CLK/TIMER2_RATE)))

; Reset vector
org 0x0000
    ljmp MainProgram

; External interrupt 0 vector (not used in this code)
org 0x0003
	reti

; Timer/Counter 0 overflow interrupt vector
org 0x000B
	reti

; External interrupt 1 vector (not used in this code)
org 0x0013
	reti

; Timer/Counter 1 overflow interrupt vector 
org 0x001B
	reti

; Serial port receive/transmit interrupt vector (not used in this code)
org 0x0023 
	reti
	
; Timer/Counter 2 overflow interrupt vector
org 0x002B
	ljmp Timer2_ISR
	
dseg at 0x30
Count1ms:     ds 2 ; Used to determine when half second has passed
Result:		  ds 2 ; Used to store temperature
x:			  ds 4 ; variable x
y:			  ds 4 ; variable y
bcd:		  ds 5 ; 10 digit bcd
Wait_Period: ds 2 ; variable WAit Period
Duty_Cycle: ds 2 ; variable Duty Cycle

bseg
mf: dbit 1
half_seconds_flag: dbit 1

CSEG

$NOLIST
$include(math32.inc)
$include(LCD_4bit.inc)
$LIST

;---------------------------------;
; Send a BCD number to PuTTY      ;
;---------------------------------;
Send_BCD mac
	push ar0
	mov r0, %0
	lcall ?Send_BCD
	pop ar0
endmac

?Send_BCD:
	push acc
	; Write most significant digit
	mov a, r0
	swap a
	anl a, #0fh
	orl a, #30h
	lcall putchar
	; write least significant digit
	mov a, r0
	anl a, #0fh
	orl a, #30h
	lcall putchar
	pop acc
	ret

Display_10_digit_BCD:
	Set_Cursor(2, 7)
	Display_BCD(bcd+4)
	Display_BCD(bcd+3)
	Display_BCD(bcd+2)
	Display_BCD(bcd+1)
	Display_BCD(bcd+0)
	ret

Send_10_digit_BCD:
	Send_BCD(bcd+4)
	Send_BCD(bcd+3)
	Send_BCD(bcd+2)
	Send_BCD(bcd+1)
	Send_BCD(bcd+0)
	mov a, #'\r'
	lcall putchar
	mov a, #'\n'
	lcall putchar
	ret
	
Timer2_Init:
	mov T2CON, #0 ; Stop timer/counter.  Autoreload mode.
	mov TH2, #high(TIMER2_RELOAD)
	mov TL2, #low(TIMER2_RELOAD)
	; Set the reload value
	mov RCAP2H, #high(TIMER2_RELOAD)
	mov RCAP2L, #low(TIMER2_RELOAD)
	; Init One millisecond interrupt counter.  It is a 16-bit variable made with two 8-bit parts
	clr a
	mov Count1ms+0, a
	mov Count1ms+1, a
	; Enable the timer and interrupts
    setb ET2  ; Enable timer 2 interrupt
    setb TR2  ; Enable timer 2
	ret

Timer2_ISR:
	clr TF2  ; Timer 2 doesn't clear TF2 automatically. Do it in ISR
	cpl P3.6 ; To check the interrupt rate with oscilloscope. It must be precisely a 1 ms pulse.
	
	; The two registers used in the ISR must be saved in the stack
	push acc
	push psw
	
	; Increment the 16-bit one mili second counter
	inc Count1ms+0    ; Increment the low 8-bits first
	mov a, Count1ms+0 ; If the low 8-bits overflow, then increment high 8-bits
	jnz Inc_Done
	inc Count1ms+1
Inc_Done:
	; Check if half second has passed
	mov a, Count1ms+0
	cjne a, #low(200), Timer2_ISR_done ; Warning: this instruction changes the carry flag!
	mov a, Count1ms+1
	cjne a, #high(200), Timer2_ISR_done
	
	; 200 milliseconds have passed.  Set a flag so the main program knows
	setb half_seconds_flag ; Let the main program know half second had passed
	; Reset to zero the milli-seconds counter, it is a 16-bit variable
	clr a
	mov Count1ms+0, a
	mov Count1ms+1, a
	
	lcall ReadTemp
	Load_X(0)	; reset X
	
	Set_Cursor(1,1)
	
	mov x, Result
	mov x+1, Result+1
	Load_Y(4004)
	; multiply by 4004 (actually 0.000004004)
	lcall mul32
	Load_Y(2730000) ; subtract 2.73
	lcall sub32
	Load_Y(100)	; times 100 degrees
	lcall mul32
	
	lcall hex2bcd ; resulting temperature is in microcelsius, send to putty
	lcall Display_10_digit_BCD
	lcall Send_10_digit_BCD
	cpl P3.7
	
Timer2_ISR_done:
	pop psw
	pop acc
	reti
	
; Configure the serial port and baud rate
InitSerialPort:
    ; Since the reset button bounces, we need to wait a bit before
    ; sending messages, otherwise we risk displaying gibberish!
    mov R1, #222
    mov R0, #166
    djnz R0, $   ; 3 cycles->3*45.21123ns*166=22.51519us
    djnz R1, $-4 ; 22.51519us*222=4.998ms
    ; Now we can proceed with the configuration
	orl	PCON,#0x80
	mov	SCON,#0x52
	mov	BDRCON,#0x00
	mov	BRL,#BRG_VAL
	mov	BDRCON,#0x1E ; BDRCON=BRR|TBCK|RBCK|SPD;
	
    ret

; Send a character using the serial port
putchar:
    jnb TI, putchar
    clr TI
    mov SBUF, a
    ret

; Send a constant-zero-terminated string using the serial port
SendString:
    clr A
    movc A, @A+DPTR
    jz SendStringDone
    lcall putchar
    inc DPTR
    sjmp SendString
SendStringDone:
    ret
 

INIT_SPI:
	setb MY_MISO ; Make MISO an input pin
	clr MY_SCLK ; For mode (0,0) SCLK is zero
	ret

DO_SPI_G:	; do not change
	push acc
	mov R1, #0 ; Received byte stored in R1
	mov R2, #8 ; Loop counter (8-bits)
	
	DO_SPI_G_LOOP:
		mov a, R0 ; Byte to write is in R0
		rlc a ; Carry flag has bit to write
		mov R0, a
		mov MY_MOSI, c
		setb MY_SCLK ; Transmit
		mov c, MY_MISO ; Read received bit
		mov a, R1 ; Save received bit in R1
		rlc a
		mov R1, a
		clr MY_SCLK
	djnz R2, DO_SPI_G_LOOP
	pop acc
ret
	
SendSerial mac
	mov DPTR, %0
    lcall SendString
endmac


ReadTemp:
	push acc
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	mov R0, #10000000B ; Single ended, read channel 0
	lcall DO_SPI_G
	mov a, R1 ; R1 contains bits 8 and 9
	anl a, #00000011B ; We need only the two least significant bits
	mov Result+1, a ; Save result high.
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov Result, R1 ; R1 contains bits 0 to 7. Save result low.
	setb CE_ADC	
	pop acc
	ret
	
MainProgram:
    mov SP, #7FH ; Set the stack pointer to the begining of idata
    lcall InitSerialPort	;SPI and serial
	lcall INIT_SPI
	lcall Timer2_Init	; Timer 2 init
	mov P2M0, #0	; SPI pin init
    mov P2M1, #0
	setb EA   ; Enable Global interrupts
    lcall LCD_4BIT	; LCD screen setup
    
    Main_loop:
		
	ljmp Main_loop	;main loop
END

sqwave_modulate: 
setb SSR_pin ; choose pin # later
;calculating how long wave stays on = Wait_Period*DutyCycle
mov x, Wait_Period
mov x+1, Wait_Period+1
mov y, Duty_Cycle
mov y+1, Duty_Cycle+1
lcall mul32 ; result stored in x 
mov a, x
cmp x, #0x255
jle x_255
Load_Y(255)
lcall div32 ; x has a/255(floor)
mov b, x
decrementing: djnz  x, x_255
x_255: Wait_Milli_Seconds(255)
cjne x, #0, decrementing
mov x, b
lcall mul32 ; Multiplies a/255(floor)*255
mov y, x
mov x, a
lcall sub32 ; x = a mod 255
Wait_Milli_Seconds(x)
clr SSR_pin 
ret