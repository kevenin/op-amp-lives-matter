import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys, time, math
import serial
import serial.tools.list_ports
import time

PORT = 'COM3'
xsize=300

tau = 0.0
currentTemp = 0.0
dTempdt = 0.0
timeElapsed = 0.0
roomTemp = 20
   
def data_gen():
    t = data_gen.t
    global currentTemp
    while True:
        t+=1
        # val=100.0*math.sin(t*2.0*3.1415/100.0)
        strin = ser.readline();
        # print(strin.decode('ascii')); 
        currentTemp = float(strin)/1000000;
        yield t, currentTemp

def run(data):
    # update the data
    t,y = data
    global timeElapsed, dTempdt, tau, roomTemp, ani
    timeElapsed = time.time() - timeElapsed
    if t>-1:
        xdata.append(t)
        ydata.append(y)
        if t>xsize: # Scroll to the left.
            ax.set_xlim(t-xsize, t)
        line.set_data(xdata, ydata)
        dydata=[0,0,0,0,0,0,0,0,0]

            #last3y = [ydata[len(ydata)-3], ydata[len(ydata)-2], ydata[len(ydata)-1]]
            #last3x = [-0.6, -0.4, -0.2] 
            #dTempdt, b = np.polyfit(last3x, last3y, 1)
        for i in range(0, 9):
            if (len(ydata) > 10):
               dydata[i] = (ydata[len(ydata)-i-1] - ydata[len(ydata)-i-2])

        dTempdt = sum(dydata)/(9*0.2);
        
        if (abs(dTempdt) >= 0.001):
            tau = (tau - (currentTemp - roomTemp)/dTempdt)/2;
            print("dT/dt = " + str(dTempdt))
            print("tau = " + str(tau))
            # pyplot.figtext(0.5, 0.5,'matplotlib', horizontalalignment='center', verticalalignment='center',transform = ax.transAxes)
        else:
            if (currentTemp < 40):
                roomTemp = currentTemp;
    timeElapsed = time.time()
    return line

def on_close_figure(event):
    sys.exit(0)

try:
    ser = serial.Serial(PORT, 115200, timeout=100)
except:
    print ('Serial port %s is not available' % PORT);


data_gen.t = -1
fig = plt.figure()
fig.canvas.mpl_connect('close_event', on_close_figure)
ax = fig.add_subplot(111)
line, = ax.plot([], [], lw=2)
ax.set_ylim(0, 200)
ax.set_xlim(0, xsize)
ax.grid()
xdata, ydata = [], []

# Important: Although blit=True makes graphing faster, we need blit=False to prevent
# spurious lines to appear when resizing the stripchart.
ani = animation.FuncAnimation(fig, run, data_gen, blit=False, interval=100, repeat=False)

plt.show()

