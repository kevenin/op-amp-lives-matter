import tkinter as tk
from tkinter import ttk

import serial
import serial.tools.list_ports
PORT = 'COM9'

try:
 ser.close();
except:
 print();
try:
 ser = serial.Serial(PORT, 115200, timeout=100)
except:
 print ('Serial port %s is not available' % PORT);
 portlist=list(serial.tools.list_ports.comports())
 print('Trying with port %s' % portlist[0][0]);
 ser = serial.Serial(portlist[0][0], 115200, timeout=100)
ser.isOpen()


root = tk.Tk()
root.title("Set Reflow Parameters")
columnHeadings = ("Heading 1", "Heading 2")

def printMsg():
    print("Ok")
def SendData():
    try:
       value = int(EnterTemp.get())     #Soak Time
       value2 = int(EnterTemp2.get())   #Soak Temp
       value3 = int(EnterTemp3.get())   #Reflow Time
       value4 = int(EnterTemp4.get())   #Reflow Temp
    except ValueError:
        print("")
        ValidLabel.config(text="Inputs are: Invalid - Values not sent")
        return   #If inputs are invalid, do not print text
    
    value =  int(EnterTemp.get())
    value2 = int(EnterTemp2.get())   #Soak Temp
    value3 = int(EnterTemp3.get())   #Reflow Time
    value4 = int(EnterTemp4.get())   #Reflow Temp
    
    if value < 0 or value > 100 or value2 < 0 or value2 > 150 or value3 < 0 or value3 > 100 or value4 < 0 or value4 > 255:
        ValidLabel.config(text="Inputs are: Invalid - Values not sent")
        return

    value =  str(value)
    value2 = str(value2)
    value3 = str(value3)
    value4 = str(value4)

    while len(value) < 4:
            value = "0" + value
    while len(value2) < 5:
            value2 = "0" + value2
    while len(value3) < 5:
            value3 = "0" + value3
    while len(value4) < 5:
            value4 = "0" + value4
    
    strStream = value + value2 + value3 + value4;
    byteStream = str.encode(value);
    ser.write(byteStream);
    byteStream = str.encode(value2);
    ser.write(byteStream);
    byteStream = str.encode(value3);
    ser.write(byteStream);
    byteStream = str.encode(value4);
    ser.write(byteStream);

  #  print(EnterTemp.get() + EnterTemp2.get() + EnterTemp3.get() + EnterTemp4.get())
    ValidLabel.config(text="Inputs are: Valid")


    
frame1 = ttk.Frame(root)
frame2 = ttk.Frame(root)
frame3 = ttk.Frame(root)
frame4 = ttk.Frame(root)

frame1.pack(side="top", fill="x")
frame2.pack(side="top", fill="both", expand=True)
frame3.pack(side="top", fill="both", expand=False)
frame4.pack(side="bottom", fill="both", expand=False)
#Row 1 Stuff
label1 = tk.Label(frame1, text="Soak Time (s)")
EnterTemp = tk.Entry(frame1, width =15, bd =5)

label2 = tk.Label(frame1, text="Soak Temp")
EnterTemp2 = tk.Entry(frame1, width =15, bd =5)

#Row 1 Pack
label1.pack(side="left")
EnterTemp.pack(side="left", padx=5)

label2.pack(side="left")
EnterTemp2.pack(side="left", padx=5)


#Row 2 Stuff
label3 = tk.Label(frame2, text="Reflow Time (s)")
EnterTemp3 = tk.Entry(frame2, width =15, bd =5)

label4 = tk.Label(frame2, text="Reflow Temp")
EnterTemp4 = tk.Entry(frame2, width =15, bd =5)

#Row 2 Pack
label3.pack(side="left", padx=5)
EnterTemp3.pack(side="left",padx=5)

label4.pack(side="left")
EnterTemp4.pack(side="left", padx=5)

#Row 3 Stuff
ValidLabel = tk.Label(frame3, text="Inputs are: ")

#Row 3 Pack
ValidLabel.pack(fill="both", side="left", pady=5)

#Row 4 Stuff
b = tk.Button(frame4, text = "Send All", command=SendData)

#Row4 Pack
b.pack(fill="both")



treeview1 = ttk.Treeview(frame4, columns=columnHeadings, show='headings',height=-1)
treeview1.pack(side="top", fill="both", expand=True)


root.mainloop()
