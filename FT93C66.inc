CSEG

FT93C66_INIT_SPI:
	setb FT93C66_MISO ; Make MISO an input pin
    clr FT93C66_SCLK  ; For mode (0,0) SCLK is zero
	ret
	
FT93C66_DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
FT93C66_DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov FT93C66_MOSI, c
    setb FT93C66_SCLK     ; Transmit
    lcall FT93C66_SmallDelay
    mov c, FT93C66_MISO   ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr FT93C66_SCLK
    lcall FT93C66_SmallDelay
    djnz R2, FT93C66_DO_SPI_G_LOOP
    pop acc
    ret

FT93C66_Wait5ms:
    push AR0
    push AR1
    mov R1, #222
    mov R0, #166
    djnz R0, $   ; 3 cycles->3*45.21123ns*166=22.51519us
    djnz R1, $-4 ; 22.51519us*222=4.998ms
    pop AR1
    pop AR0
    ret
 
FT93C66_SmallDelay:
    nop ; 45 ns @ 22.1148 MHz
    nop ; 90 ns
    nop ; 135 ns 
    nop ; 180 ns 
    nop ; 225 ns 
    nop ; 270 ns
    ret
    
; EWEN: 1001 1xxxxxxx
FT93C66_Write_Enable:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov R0, #1001B ; Send start bit and op code
	lcall FT93C66_DO_SPI_G
	mov R0, #10000000B ; Send enable bit
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms
	ret
	
; EWDS: 1000 0xxxxxxx
FT93C66_Write_Disable:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov R0, #1000B ; Send start bit and op code
	lcall FT93C66_DO_SPI_G
	mov R0, #00000000B ; Send enable bit
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms
	ret

; Address to read passed in dptr (dpl and dph)
; Value read, returned in accumulator
; READ: 110[A8] [A7 downto A0]
FT93C66_Read:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov a, #1100B
	orl a, dph
	mov R0, a ; Send start bit, op code, and MSB of address
	lcall FT93C66_DO_SPI_G
	mov R0, dpl ; Send LSB of address
	lcall FT93C66_DO_SPI_G
	mov R0, #11111111B ; Dummy value to receive data
	lcall FT93C66_DO_SPI_G
	mov a, R1
	clr FT93C66_CE ;  De-activate the EEPROM.
	ret

; Address to erase passed in dptr (dpl and dph)
; ERASE: 111[A8] [A7 downto A0]
FT93C66_Erase:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov a, #1110B
	orl a, dph
	mov R0, a ; Send start bit, op code, and MSB of address
	lcall FT93C66_DO_SPI_G
	mov R0, dpl ; Send LSB of address
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms 
	ret
	
; ERAL: 1001 0xxxxxxx
FT93C66_Erase_All:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov R0, #1001B ; Send start bit & op code
	lcall FT93C66_DO_SPI_G
	mov R0, #00000000B ; rest of op code
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms 
	lcall FT93C66_Wait5ms ; Microchip part takes a bit longer to do a full erase
	ret

; Address to write passed in dptr (dpl and dph)
; Value to write passed in accumulator
; WRITE: 101[A8] [A7 downto A0] 
FT93C66_Write:
    push acc
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov a, #1010B
	orl a, dph
	mov R0, a ; Send start bit, op code, and MSB of address
	lcall FT93C66_DO_SPI_G
	mov R0, dpl ; Send LSB of address
	lcall FT93C66_DO_SPI_G
	pop acc
	mov R0, a ; value to write
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms
	ret
	
; WRAL: 1000 1xxxxxxx
FT93C66_Write_All:
	setb FT93C66_CE  ; Activate the EEPROM.
	lcall FT93C66_SmallDelay
	mov R0, #1000B ; Send start bit & op code
	lcall FT93C66_DO_SPI_G
	mov R0, #10000000B ; rest of op code
	lcall FT93C66_DO_SPI_G
	mov R0, a ; value to write
	lcall FT93C66_DO_SPI_G
	clr FT93C66_CE ;  De-activate the EEPROM.
	lcall FT93C66_Wait5ms 
	lcall FT93C66_Wait5ms ; The microchip part takes a bit longer to do a write all
	lcall FT93C66_Wait5ms 
	ret



    Save_Configuration:
	lcall FT93C66_Write_Enable
	mov DPTR, #0
	Save variables
	mov a, SoakTemp
	lcall FT93C66_Write
	inc DPTR
	mov a, SoakTime
	lcall FT93C66_Write
	inc DPTR
	mov a, ReflowTemp
	lcall FT93C66_Write 
	inc DPTR
	mov a, ReflowTime
	lcall FT93C66_Write
	inc DPTR
	mov a, #0x55 ; First key value
	lcall FT93C66_Write
	inc DPTR
	mov a, #0xAA ; Second key value
	lcall FT93C66_Write
	lcall FT93C66_Write_Disable
	ret

Load_Configuration:
	mov dptr, #0x0004 ;First key value location.  Must be 0x55
	lcall FT93C66_Read
	cjne a, #0x55, Load_Defaults
	inc dptr ; Second key value location.  Must be 0xaa
	lcall FT93C66_Read
	cjne a, #0xaa, Load_Defaults

	; Keys are good. Load saved values.
	mov dptr, #0x0000
	lcall FT93C66_Read
	mov SoakTemp, a
	inc dptr
	lcall FT93C66_Read
	mov SoakTime, a
	inc dptr
	lcall FT93C66_Read
	mov ReflowTime, a
	inc dptr
	lcall FT93C66_Read
	mov ReflowTime, a
	ret