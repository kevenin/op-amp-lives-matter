$NOLIST
CSEG

Poll_BCD mac
	push ar0
	jb skippy, $+12
		lcall ?Poll_BCD
	jb skippy, $+6		;check if param button is pressed
		mov %0, r0
	pop ar0
endmac

?Poll_BCD:
	push acc
	mov r0, #0x55
	lcall pollchar
	jb skippy, done_poll
		; Write most significant digit
		anl a, #0x0F
		swap a
		mov r0, a
		
	lcall pollchar
	jb skippy, done_poll
		; write least significant digit
		anl a, #0x0F
		add a, r0
		mov r0, a
		
	done_poll:
	pop acc
	ret

pollchar:
	read_button(param_button, noskippy)		;check if param button is pressed
		setb skippy
		ret
	noskippy:
		jnb RI, pollchar
		mov a, SBUF
		clr RI
		ret

reset_bcd:
	mov bcd, #0
	mov bcd+1, #0
	mov bcd+2, #0
	mov bcd+3, #0
	mov bcd+4, #0
	ret
	
Poll_params:	;these macros will quit if screen is not on state 5
	push acc
	lcall reset_bcd
	clr skippy
	
	Poll_BCD(bcd)		;00
	Poll_BCD(bcd)		;soak time
	lcall bcd2hex
	jnb skippy, $+6
		ljmp no_params
	mov soaktime, x
	
	lcall reset_bcd
	Poll_BCD(bcd+1)		;00
	Poll_BCD(bcd)		;soak temp
	lcall bcd2hex
	
	jnb skippy, $+6
		ljmp no_params
	mov soaktemp, x
	
	lcall reset_bcd
	Poll_BCD(bcd)		;00
	Poll_BCD(bcd)		;reflow time
	lcall bcd2hex
	jnb skippy, $+6
		ljmp no_params
	mov reflowtime, x
	
	lcall reset_bcd
	Poll_BCD(bcd+1)		;00
	Poll_BCD(bcd)		;reflow temp
	lcall bcd2hex
	jnb skippy, $+6
		ljmp no_params
	mov reflowtemp, x
	mov reflowtemp+1, x+1
	
	setb speaker_on	; beep
		Wait_Milli_Seconds(#200)
	clr speaker_on
		Wait_Milli_Seconds(#200)
	setb speaker_on	; beep
		Wait_Milli_Seconds(#200)
	clr speaker_on
	
	no_params:
		clr skippy
	pop acc
	ret

$LIST