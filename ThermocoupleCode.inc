
$NOLIST

CSEG

; Requires: x, y, temp, sum

; To use and read temp: loop ?Average_ADC_Channel 

Average_ADC_Channel:
		Load_x(0)
		mov sum+3, x+3
		mov sum+2, x+2
		mov sum+1, x+1
		mov sum+0, x+0
		
		mov R5, #20
	
	Sum_loop0:
		lcall ?Read_ADC_Channel
		
		mov y+3, sum+3
		mov y+2, sum+2
		mov y+1, sum+1
		mov y+0, sum+0
	
		lcall add32
		
		mov sum+3, x+3
		mov sum+2, x+2
		mov sum+1, x+1
		mov sum+0, x+0	
		
		djnz R5, Sum_loop0
	
		load_y(20)
		mov x+3, sum+3
		mov x+2, sum+2
		mov x+1, sum+1
		mov x+0, sum+0
		lcall div32
		
		lcall ?CalculateTemp
		
		lcall hex2bcd

		Send_BCD(bcd+4)
		Send_BCD(bcd+3)
		Send_BCD(bcd+2)
		Send_BCD(bcd+1)
		Send_BCD(bcd+0)

		mov DPTR, #Hello_World ;Modified to send new line
		lcall SendString

		
		mov temp+0, x+0
		mov temp+1, x+1
		mov temp+2, x+2
		mov temp+3, x+3
	ret

?Read_ADC_Channel:
		clr CE_ADC
		mov R0, #00000001B ; Start bit:1
		lcall DO_SPI_G
		mov R0, #10000000B ; Single ended, read channel 0
		lcall DO_SPI_G
		mov a, R1 ; R1 contains bits 8 and 9
		anl a, #00000011B ; We need only the two least significant bits
		mov Result+1, a ; Save result high.
		mov R0, #55H ; It doesn't matter what we transmit...
		lcall DO_SPI_G
		mov Result, R1 ; R1 contains bits 0 to 7. Save result low.
		setb CE_ADC
		lcall ?Delay
		mov x+0, (Result)
		mov x+1, (Result+1)
		mov x+2, #0
		mov x+3, #0
	ret

?Delay: 
    Wait_Milli_Seconds(#1)
    ret

?CalculateTemp: 	
		
		Load_y(0)
		;Load_x(0)
		
		Load_y(4104)	;voltage linear fit
		lcall mul32
		Load_y(1023)
		lcall div32
		
		Load_y(5)		;voltage offset
		lcall add32
		
		
		Load_y(10)		;linear fit to temperature
		lcall mul32
		Load_y(166) 		
		lcall div32
		
		Load_y(0)		;temperature offset
		;Load_y(25)		
		mov y, ref_temp
		lcall add32
    ret

SetRoomTemp:
	;do serial read from channel 1
	push acc
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	mov R0, #10010000B ; Single ended, read channel 0
	lcall DO_SPI_G
	mov a, R1 ; R1 contains bits 8 and 9
	anl a, #00000011B ; We need only the two least significant bits
	mov Result+1, a ; Save result high.
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov Result, R1 ; R1 contains bits 0 to 7. Save result low.
	setb CE_ADC	

	Load_X(0)	; reset X
	
	mov x, Result
	mov x+1, Result+1
	Load_Y(4004)
	; multiply by 4004 (actually 0.000004004)
	lcall mul32
	Load_Y(2730000) ; subtract 2.73
	lcall sub32
	Load_Y(10000)	; times 100 degrees and divide by 1,000,000 micro
	lcall div32
	
	mov ref_temp, x ; move the first byte of x to reference
	
	pop acc
	ret

ReadPotent:
	push acc
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	mov R0, #10100000B ; Single ended, read channel 2
	lcall DO_SPI_G
	mov a, R1 ; R1 contains bits 8 and 9
	anl a, #00000011B ; We need only the two least significant bits
	mov Result+1, a ; Save result high.
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov Result, R1 ; R1 contains bits 0 to 7. Save result low.
	setb CE_ADC	
	
	
	mov Potentiometer, result
	mov Potentiometer+1, result+1
	
	pop acc
	ret	

$LIST